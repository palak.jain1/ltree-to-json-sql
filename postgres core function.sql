--PostgreSQL: Script to search any Text from the Stored Function

--Using pg_proc:

select 
	proname AS FunctionName	
from pg_proc where prosrc like '%Your_Text%';

--Using information_schema.routines:

select 
	routine_catalog AS DatabaseName
	,routine_schema AS SchemaName
	,routine_name AS FunctionName
	,routine_type AS ObjectType
from information_schema.routines 
where routine_definition like '%Your_Text%'
---------------------------------------------------------------------------------------------------------------------
-- Delete all duplicates rows except one in SQL Server
--Create a sample table:
CREATE TABLE tbl_RemoveDuplicate
(	
	ID INTEGER PRIMARY KEY
	,Name VARCHAR(150)
);
--Insert few duplicate records:
INSERT INTO tbl_RemoveDuplicate VALUES
(1,'ABC'),(2,'XYZ')
,(3,'XYZ'),(4,'RFQ')
,(5,'PQR'),(6,'EFG')
,(7,'EFG'),(8,'ABC');
--Except one, Delete all duplicate records:
DELETE FROM tbl_RemoveDuplicate
WHERE ID IN 
(SELECT ID
FROM (SELECT id,
	     ROW_NUMBER() OVER (partition BY Name ORDER BY ID) AS RowNumber
     FROM tbl_RemoveDuplicate) AS T
WHERE T.RowNumber > 1);
---------------------------------------------------------------------------------------------------------------------
-- How can you get the active users connected?
select * from pg_stat_activity;
--------------------------------------------------------------------------------------------------------------------
-- Check Postgres access for a user

SELECT table_catalog, table_schema, table_name, privilege_type
FROM   information_schema.table_privileges 
WHERE  grantee = 'MY_USER'
