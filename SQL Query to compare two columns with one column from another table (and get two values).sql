Is it possible to, based on the ID value of two columns from table1, get their equivalent in table2?

My table1 looks like this:

id | origin | destiny
-- | ------ | -------
1  | 2      | 3
2  | 4      | 5
and table2, like this:

id | name
-- | ----
1  | bla
2  | asd
3  | dfg
4  | qwe
5  | tle
And I want to get something like this:

id | origin | destiny | nameOrigin | nameDestiny
-- | ------ | ------- | ---------- | -----------
1  | 2      | 3       | asd        | dfg
2  | 4      | 5       | qwe        | tle


============================ANSWER======================================


You can join the second table twice:

SELECT      t1.id,
            t1.origin, 
            t1.destiny,
            o.name as nameOrigin,
            d.name as nameDestiny
FROM        table1 t1 
INNER JOIN  table2 o ON t1.origin = o.id
INNER JOIN  table2 d ON t1.destiny = d.id
NB: "destiny" is not the same thing as "destination".
