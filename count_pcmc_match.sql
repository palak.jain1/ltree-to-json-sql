-- FUNCTION: pcmc_survey.fn_pcmc_spatial_summary(text)
-- DROP FUNCTION IF EXISTS pcmc_survey.fn_pcmc_spatial_summary(text);
CREATE OR REPLACE FUNCTION pcmc_survey.fn_pcmc_spatial_summary (datadetail text)
	RETURNS text
	LANGUAGE 'plpgsql'
	COST 100 VOLATILE
	SECURITY DEFINER PARALLEL UNSAFE
	AS $BODY$
	/*
	 SELECT pcmc_survey.fn_pcmc_spatial_summary( '{"type":1}' )
	 SELECT pcmc_survey.fn_pcmc_spatial_summary('{"property_type":"Building",
	 "status":"Access Denied,Demolished,Door Accessible,Door Locked,Missing,Permanent Locked,Temporary Structure,Under Construction",
	 "zone":"0","from_date":"01/07/2021","to_date":"17/01/2022","type":2}')
	 SELECT pcmc_survey.fn_pcmc_spatial_summary('{"property_type":"Building",
	 "status":"Access Denied,Demolished,Door Accessible,Door Locked,Missing,Permanent Locked,Under Construction",
	 "zone":"0","from_date":"08/06/2021","to_date":"03/12/2022","type":2}')
	 SELECT pcmc_survey.fn_pcmc_spatial_summary('{"property_type":"Property",
	 "status":"Access Denied,Demolished,Door Accessible,Door Locked,Missing,Permanent Locked,Under Construction",
	 "zone":"0","from_date":"01/08/2021","to_date":"04/01/2022","type":3}')  
	 SELECT pcmc_survey.fn_pcmc_spatial_summary( '{"summary_type":"surveyor","from_date":"01/09/2021","to_date":"03/12/2021",
	 "property_type":"Building","type":4}' )
	 - gut - grid - surveyor - zone
	 */
DECLARE
	-- declare variable
	datadetails json;
	qry text;
	qry_ot json;
	graph_data json;
	property_data json;
	building_data json;
	pro_status_data json;
	gut_no_data json;
	zone_no_data json;
	grid_no_data json;
	TYPE int;
	property_type text;
	status text;
	zone integer;
	gut text;
	grid text;
	from_date text;
	to_date text;
	cm_prop integer;
	cm_prop_pen integer;
	tot_prop integer;
	cm_bild integer;
	cm_bild_pen integer;
	tot_bild integer;
	status_data text;
	cm_bild_input text;
	cm_bild_pen_input text;
	tot_bild_input text;
	graph_data_input text;
	cm_prop_input text;
	cm_prop_pen_input text;
	tot_prop_input text;
	tot_leg_prop_input text;
	tot_leg_prop int;
	extent text;
	extent_input text;
	summary_type text;
	col_name text;
	summary_grid_input text;
	summary_grid json;
	alias_name text;
	date_cond text;
	date_lesseq_cond text;
    no_status text;
BEGIN
	datadetails := datadetail::json;
	RAISE NOTICE 'input_param %', datadetails;
	TYPE := (datadetails ->> 'type');
	property_type := (datadetails ->> 'property_type');
	status := (datadetails ->> 'status');
	zone := (datadetails ->> 'zone');
	gut := (datadetails ->> 'gut');
	grid := (datadetails ->> 'grid');
	from_date := datadetails ->> 'from_date';
	to_date := datadetails ->> 'to_date';
	summary_type := datadetails ->> 'summary_type';
	RAISE NOTICE 'property_type -%,status -%,zone -%,gut -%,grid -%,from_date -%,to_date -%', property_type, status, zone, gut, grid, from_date, to_date;
	status_data := 'SELECT unnest(string_to_array((' || QUOTE_LITERAL(status) || '),'',''))';
	RAISE NOTICE 'status_data -%', status_data;
	date_cond := 'TO_DATE(record_datetime, ''DD/MM/YYYY'') >= TO_DATE(' || QUOTE_LITERAL(from_date) || ', ''DD/MM/YYYY'')
		  AND TO_DATE(record_datetime, ''DD/MM/YYYY'') <= TO_DATE('||quote_literal(to_date)||', ''DD/MM/YYYY'')';
	date_lesseq_cond := 'to_date(record_datetime,''DD/MM/YYYY'') <= TO_DATE(' || QUOTE_LITERAL(to_date) || ', ''DD/MM/YYYY'')';
    no_status := 'No Status';
	qry := '';
	IF (zone = 0 OR zone IS NULL) THEN
		qry := '1=1';
		RAISE NOTICE 'qry -%', qry;
	ELSE
		qry := qry || ' zone_no = ' || QUOTE_LITERAL(zone) || '';
		RAISE NOTICE 'qry -%', qry;
		IF (gut = '' OR gut IS NULL) THEN
			qry := qry;
			RAISE NOTICE 'qry -%', qry;
		ELSE
			qry := qry || ' AND gut_no IN (SELECT unnest(string_to_array((' || QUOTE_LITERAL(gut) || '),'','')))';
			RAISE NOTICE 'qry -%', qry;
			IF (grid = '' OR grid IS NULL) THEN
				qry := qry;
				RAISE NOTICE 'qry -%', qry;
			ELSE
				qry := qry || ' AND grid_id IN (SELECT unnest(string_to_array((' || QUOTE_LITERAL(grid) || '),'','')))';
				RAISE NOTICE 'qry -%', qry;
			END IF;
		END IF;
	END IF;
	IF (type = 1) THEN
		RAISE NOTICE 'type 1';
		EXECUTE 'select json_agg(json_build_object(''name'',door_status)) FROM (
	select distinct(door_status) FROM (select distinct(door_status) from pcmc_survey.tbl_property
	union all select distinct(door_status) from pcmc_survey.tbl_buildings)q1 WHERE door_status is not null )q' INTO pro_status_data;
		EXECUTE 'select json_agg(json_build_object(''id'', gut_no,''name'',name2, ''zoneid'', zone_no )) FROM (
	select gut_no, (''gut''||'' ''||gut_no) name2 , zone_no from pcmc_survey.building_w_grid_id_3857
	where gut_no is not null group by gut_no, zone_no)q' INTO gut_no_data;
		EXECUTE 'select json_agg(json_build_object(''id'',zone_no,''name'',zone_name)) FROM (
	select distinct zone_no,zone_name from pcmc_survey.building_w_grid_id_3857
	where zone_name is not null) q' INTO zone_no_data;
		EXECUTE 'select json_agg(json_build_object(''id'', grid_id,''name'', grid_id,''gutid'', gut_no )) FROM (
	select grid_id, gut_no from pcmc_survey.building_w_grid_id_3857
	where grid_id is not null group by gut_no, grid_id)q' INTO grid_no_data;
		RETURN JSON_BUILD_OBJECT('data', JSON_BUILD_OBJECT('status', pro_status_data, 'gut', gut_no_data, 'zone', zone_no_data, 'grid', grid_no_data));
	
    ELSIF (type = 2) THEN
		RAISE NOTICE 'type 2 building';
		cm_bild_input := 
            'WITH cm_bild_input AS (SELECT t2.gis_id,t1.zone_no,t1.gut_no,t1.grid_id,
                    coalesce(t2.door_status,'||QUOTE_LITERAL(no_status)||') door_status,
			        to_char(t2.survey_date::date,''DD/MM/YYYY'') record_datetime
			        FROM pcmc_survey.building_w_grid_id_3857 t1
			        JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
            )
            SELECT COALESCE(SUM(cmpled_bild),0) cmpled_bild 
            FROM (	SELECT gis_id,count(distinct (gis_id)) cmpled_bild 
                FROM ( SELECT gis_id 
                    FROM (
                        SELECT gis_id,zone_no,gut_no,grid_id,door_status,record_datetime FROM cm_bild_input
                        )q1 WHERE '||date_cond||' AND '||qry||' AND door_status IN ('||status_data||')
			                AND zone_no is not null and gut_no is not null and grid_id is not null
                    )q2 GROUP BY gis_id
                )q3';
		RAISE NOTICE 'cm_bild_input - %', cm_bild_input;
		EXECUTE cm_bild_input INTO cm_bild;
		cm_bild_pen_input := 
            'WITH cm_bild_pen_input AS (SELECT t2.gis_id,t1.zone_no,t1.gut_no,t1.grid_id,
                    coalesce(t2.door_status,'||QUOTE_LITERAL(no_status)||') door_status,
                    to_char(t2.survey_date::date,''DD/MM/YYYY'') record_datetime
                    FROM pcmc_survey.building_w_grid_id_3857 t1
                    RIGHT JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
            )
            SELECT COALESCE(SUM(cmpled_bild),0) cmpled_bild 
            FROM ( SELECT gis_id,count(distinct (gis_id)) cmpled_bild 
                FROM ( SELECT gis_id 
                    FROM (
                        SELECT gis_id,zone_no,gut_no,grid_id,door_status,record_datetime FROM cm_bild_pen_input
                        )q1 WHERE '||date_lesseq_cond||' AND '||qry||' AND door_status is not null
                        AND zone_no is not null and gut_no is not null and grid_id is not null
                    )q2 GROUP BY gis_id
                )q3';
		RAISE NOTICE 'cm_bild_pen_input - %', cm_bild_pen_input;
		EXECUTE cm_bild_pen_input INTO cm_bild_pen;
		tot_bild_input := 
            'WITH tot_bild_input AS (SELECT distinct t1.building_i
                FROM pcmc_survey.building_w_grid_id_3857 t1
                LEFT JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
                WHERE '||qry||'
                AND t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null
            )
            SELECT COALESCE(SUM(total_building),0) total_building 
            FROM (
                    SELECT building_i, count(building_i) total_building 
                    FROM (
                        SELECT building_i FROM tot_bild_input  
                    )q1 GROUP BY building_i
                )q2';
		RAISE NOTICE 'tot_bild_input - %', tot_bild_input;
		EXECUTE tot_bild_input INTO tot_bild;
		EXECUTE 'SELECT json_build_object(''total_property'',COALESCE(' || tot_bild || ',0),
									''completed_property'',COALESCE('||cm_bild||',0),
									''pending_property'',COALESCE(('||tot_bild||'-'||cm_bild_pen||'),0),
									''qc_bygenesys'',0)' INTO building_data;
		RAISE NOTICE 'final raise notice - %', qry;
		graph_data_input := 'SELECT json_agg(row_to_json(q5)) FROM (
SELECT record_datetime,
SUM(COALESCE("Access Denied",0)) access_denied,
SUM(COALESCE("Demolished",0)) demolished,
SUM(COALESCE("Door Accessible",0)) door_accessible,
SUM(COALESCE("Door Locked",0)) door_locked,
SUM(COALESCE("Missing",0)) missing,
SUM(COALESCE("New",0)) neww,
SUM(COALESCE("'||no_status||'",0)) no_status,	
SUM(COALESCE("Owner Not Available",0)) owner_not_available,
SUM(COALESCE("Permanent Locked",0)) permanent_locked,
SUM(COALESCE("Temporary Structure",0)) temporary_structure,
SUM(COALESCE("Under Construction",0))  under_construction,
SUM((COALESCE("Access Denied",0)+COALESCE("Demolished",0)+COALESCE("Door Accessible",0)+COALESCE("Door Locked",0)+COALESCE("Missing",0)
+COALESCE("Owner Not Available",0)+COALESCE("Permanent Locked",0)+COALESCE("Temporary Structure",0)+COALESCE("Under Construction",0)
+COALESCE("'||no_status||'",0))) total
FROM crosstab
($$ 
WITH graph1 AS (
		SELECT t3.gis_id,t1.zone_no,t1.gut_no,t1.grid_id,
        coalesce(t3.door_status,'||QUOTE_LITERAL(no_status)||') door_status,
		to_char(t3.survey_date::date,''DD/MM/YYYY'') record_datetime
		FROM pcmc_survey.building_w_grid_id_3857 t1
		JOIN pcmc_survey.buildings_others_en t3 ON t1.building_i = t3.gis_id 
    )
    SELECT record_datetime,door_status,count(distinct (gis_id)) cmpled_pro
        FROM ( SELECT gis_id,door_status,record_datetime FROM 
                ( SELECT gis_id,zone_no,gut_no,grid_id,door_status, record_datetime FROM graph1
            )q1 WHERE '||date_cond||' AND '||qry||'
		AND door_status IN ('||status_data||')
		AND zone_no is not null and gut_no is not null and grid_id is not null
        )q2 GROUP BY door_status,record_datetime

UNION ALL

(WITH graph2 AS (SELECT distinct to_char(t2.survey_date::date,''DD/MM/YYYY'') record_datetime,
			t2.gis_id,''New'' door_status, 1 as cmpled_pro			
			FROM pcmc_survey.building_w_grid_id_3857 t1
			JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
			WHERE '||qry||'
			AND t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null
			AND t1.building_i ilike ''N-%'' )
		SELECT record_datetime, door_status, count(cmpled_pro) cmpled_pro FROM
			(SELECT record_datetime,gis_id,door_status,cmpled_pro FROM graph2)q1 
			WHERE '||date_cond||' group by door_status,record_datetime)
			$$,
$$select unnest(string_to_array(''Access Denied,Temporary Structure,Owner Not Available,Door Locked,Missing,Permanent Locked,Under Construction,Demolished,Door Accessible,New,'||no_status||''','','')) AS sq ORDER BY sq$$)
AS ct(record_datetime text, "Access Denied" numeric, "Demolished" numeric, "Door Accessible" numeric,
"Door Locked" numeric,"Missing" numeric,"New" numeric,"'||no_status||'" numeric,"Owner Not Available" numeric,"Permanent Locked" numeric,
"Temporary Structure" numeric, "Under Construction" numeric)group by record_datetime order by 1)q5';
		RAISE NOTICE 'final graph_data - %', graph_data_input;
		EXECUTE graph_data_input INTO graph_data;
		IF (graph_data IS NULL) THEN
			graph_data := '[]';
		END IF;
		extent_input := 'SELECT (ST_XMin(bb)||'',''|| ST_YMin(bb)||'',''|| ST_XMax(bb)||'',''|| ST_YMax(bb)) extent
                    FROM (SELECT ST_Extent(thegeom) bb FROM (
                        SELECT ST_Union(geom) AS thegeom FROM (
                            SELECT distinct t1.building_i,geom FROM pcmc_survey.building_w_grid_id_3857 t1 WHERE  '||qry||') q1  
                        )q2
                )bx';
		EXECUTE extent_input INTO extent;
		RETURN JSON_BUILD_OBJECT('data', building_data, 'graph_data', graph_data, 'extent', extent);
	ELSIF (type = 3) THEN
		RAISE NOTICE 'type 3 property';
		cm_prop_input := 
            'WITH cm_prop_input AS (
                    SELECT t3.property_no,t2.gis_id,t1.zone_no,t1.gut_no,t1.grid_id,
                    coalesce(t3.door_status,'||QUOTE_LITERAL(no_status)||') door_status,
                    to_char(t3.survey_date::date,''DD/MM/YYYY'') record_datetime
                    FROM pcmc_survey.building_w_grid_id_3857 t1
                    JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
                    JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
                )
            SELECT COALESCE(SUM(cmpled_pro),0) cmpled_pro 
            FROM ( SELECT gis_id,count(distinct ( property_no,gis_id)) cmpled_pro, 0 qc_bygenesys 
                FROM ( SELECT property_no,gis_id 
                    FROM (
                            SELECT property_no,gis_id,zone_no,gut_no,grid_id,door_status,record_datetime FROM cm_prop_input
                        )q1 WHERE '||date_cond||'
                        AND '||qry||' AND door_status IN ('||status_data||')
                        AND zone_no is not null and gut_no is not null and grid_id is not null
                    )q2 GROUP BY gis_id
                )q3';
		RAISE NOTICE 'cm_prop_input - %', cm_prop_input;
		EXECUTE cm_prop_input INTO cm_prop;
		cm_prop_pen_input := 
            'WITH cm_prop_pen_input AS (
                    SELECT t3.property_no,t2.gis_id,t1.zone_no,t1.gut_no,t1.grid_id,
                    coalesce(t3.door_status,'||QUOTE_LITERAL(no_status)||') door_status,
                    to_char(t3.survey_date::date,''DD/MM/YYYY'') record_datetime
                    FROM pcmc_survey.building_w_grid_id_3857 t1
                    JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
                    JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
                )
            SELECT COALESCE(SUM(cmpled_pro),0) cmpled_pro 
            FROM ( SELECT gis_id,count(distinct ( property_no,gis_id)) cmpled_pro, 0 qc_bygenesys 
                FROM ( SELECT property_no,gis_id 
                    FROM (
                            SELECT property_no,gis_id,zone_no,gut_no,grid_id,door_status, record_datetime FROM cm_prop_pen_input
                            )q1 WHERE '||date_lesseq_cond||' 
                                AND '||qry||' AND door_status is not null
                                AND zone_no is not null and gut_no is not null and grid_id is not null
                    )q2 GROUP BY gis_id
                )q3';
		RAISE NOTICE 'cm_prop_pen_input - %', cm_prop_pen_input;
		EXECUTE cm_prop_pen_input INTO cm_prop_pen;
		--SELECT * FROM pcmc_survey.tbl_buildings
		tot_prop_input := 'SELECT COALESCE(SUM(total_property),0) total_property FROM (
                            SELECT distinct t1.building_i,
                            (COALESCE(t2.total_properties,0)) total_property
                            FROM pcmc_survey.building_w_grid_id_3857 t1
                            LEFT JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
                            LEFT JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
                            WHERE '||qry||' AND t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null)q1';
		EXECUTE tot_prop_input INTO tot_prop;
		tot_leg_prop_input := 'SELECT COALESCE(SUM(total_property),0) total_property FROM (
                                SELECT distinct t1.building_i,
                                (COALESCE(t2.total_properties,0)) total_property
                                FROM pcmc_survey.building_w_grid_id_3857 t1
                                LEFT JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
                                LEFT JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
                                WHERE '||qry||' AND t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null
                                AND t1.building_i not ilike ''N%'' )q1';
		EXECUTE tot_leg_prop_input INTO tot_leg_prop;
		RAISE NOTICE 'tot_prop_input - %', tot_prop_input;
		EXECUTE 'SELECT json_build_object(''total_property'',COALESCE(' || tot_prop || ',0),
                ''completed_property'',COALESCE('||cm_prop||',0),
                ''total_legacy_property'',COALESCE('||tot_leg_prop||',0),
                ''pending_property'',COALESCE(('||tot_prop||'-'||cm_prop_pen||'),0),
                ''qc_bygenesys'',0)' INTO property_data;
		RAISE NOTICE 'property_data -%', property_data;
		graph_data_input := 'SELECT json_agg(row_to_json(q5)) FROM (
SELECT record_datetime,
SUM(COALESCE("Access Denied",0)) access_denied,
SUM(COALESCE("Demolished",0)) demolished,
SUM(COALESCE("Door Accessible",0)) door_accessible,
SUM(COALESCE("Door Locked",0)) door_locked,
SUM(COALESCE("Missing",0)) missing,
SUM(COALESCE("New",0)) neww,
SUM(COALESCE("'||no_status||'",0)) no_status,			
SUM(COALESCE("Owner Not Available",0)) owner_not_available,
SUM(COALESCE("Permanent Locked",0)) permanent_locked,
SUM(COALESCE("Temporary Structure",0)) temporary_structure,
SUM(COALESCE("Under Construction",0))  under_construction,
SUM((COALESCE("Access Denied",0)+COALESCE("Demolished",0)+COALESCE("Door Accessible",0)+COALESCE("Door Locked",0)+COALESCE("Missing",0)
+COALESCE("Owner Not Available",0)+COALESCE("Permanent Locked",0)+COALESCE("Temporary Structure",0)+COALESCE("Under Construction",0)
+COALESCE("'||no_status||'",0))) total
FROM crosstab
($$ 
WITH graph1 AS (
    SELECT t3.property_no,t3.gis_id,t1.zone_no,t1.gut_no,t1.grid_id,
    coalesce(t3.door_status,'||QUOTE_LITERAL(no_status)||') door_status,
    to_char(t3.survey_date::date,''DD/MM/YYYY'') record_datetime
    FROM pcmc_survey.building_w_grid_id_3857 t1
    JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
    JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
    )
        SELECT record_datetime,door_status,count(distinct (gis_id,property_no)) cmpled_pro
            FROM ( SELECT property_no,gis_id,door_status,record_datetime FROM 
                    (
                        SELECT property_no,gis_id,zone_no,gut_no,grid_id,door_status,record_datetime FROM graph1
                    )q1 WHERE '||date_cond||' 
                        AND '||qry||'
                        AND door_status IN ('||status_data||')
                )q2 GROUP BY door_status,record_datetime 

UNION ALL
(WITH graph2 AS (SELECT t1.building_i,
					''New'' door_status,
					property_no,
					to_char(t2.survey_date::date,''DD/MM/YYYY'') record_datetime
				FROM pcmc_survey.building_w_grid_id_3857 t1
					LEFT JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
					LEFT JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
					WHERE '||qry||' AND t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null
					AND t1.building_i ilike ''N-%'' AND property_no is not null )
					
				SELECT	record_datetime, door_status, COUNT(distinct (building_i,property_no)) cmpled_pro FROM (
				SELECT building_i,door_status,property_no,record_datetime FROM graph2 )q1 
				WHERE '||date_cond||' GROUP BY door_status,record_datetime )
				$$,
$$select unnest(string_to_array(''Access Denied,Temporary Structure,Owner Not Available,Door Locked,Missing,Permanent Locked,Under Construction,Demolished,Door Accessible,New,'||no_status||''','','')) AS sq ORDER BY sq$$)
AS ct(record_datetime text, "Access Denied" numeric, "Demolished" numeric, "Door Accessible" numeric,
"Door Locked" numeric,"Missing" numeric,"New" numeric,"'||no_status||'" numeric,"Owner Not Available" numeric,"Permanent Locked" numeric,
"Temporary Structure" numeric, "Under Construction" numeric)group by record_datetime order by 1)q5';
		EXECUTE graph_data_input INTO graph_data;
		IF (graph_data IS NULL) THEN
			graph_data := '[]';
		END IF;
		RAISE NOTICE 'final graph_data - %', graph_data_input;
		extent_input := 'SELECT (ST_XMin(bb)||'',''|| ST_YMin(bb)||'',''|| ST_XMax(bb)||'',''|| ST_YMax(bb)) extent
FROM (SELECT ST_Extent(thegeom) bb FROM (
SELECT ST_Union(geom) AS thegeom FROM (
SELECT distinct t1.building_i,geom FROM pcmc_survey.building_w_grid_id_3857 t1 WHERE  '||qry||') q1  
)q2
 )bx';
		EXECUTE extent_input INTO extent;
		RETURN JSON_BUILD_OBJECT('data', property_data, 'graph_data', graph_data, 'extent', extent);
	ELSIF (type = 4) THEN
		IF (summary_type = 'zone') THEN
			col_name := 'zone_no';
			alias_name := 'Zone';
		ELSIF (summary_type = 'gut') THEN
			col_name := 'gut_no';
			alias_name := 'Gut';
		ELSIF (summary_type = 'grid') THEN
			col_name := 'grid_id';
			alias_name := '';
		ELSIF (summary_type = 'surveyor') THEN
			col_name := 'username';
		ELSE
			RETURN JSON_BUILD_OBJECT('data', 'incorrect input');
		END IF;
		IF (summary_type = 'surveyor') THEN
			RAISE NOTICE 'summary_type -%', summary_type;
summary_grid_input := '
WITH bld_summary as ( WITH bld_summary_in AS (SELECT t2.username,t2.gis_id,t2.door_status,
		to_char(t2.survey_date::date,''DD/MM/YYYY'') record_datetime
		FROM pcmc_survey.building_w_grid_id_3857 t1
		JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
		WHERE --t2.door_status is not null and 
        t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null)
SELECT count(distinct (gis_id)) cmpled_bild,username FROM (
	SELECT username,gis_id FROM (
		SELECT username,gis_id,door_status,record_datetime FROM bld_summary_in
		)q1 WHERE '||date_cond||'
	)q2 GROUP BY username
),
bld_summary_pen as (WITH bld_summary_pen_in AS (
		SELECT t2.gis_id,t2.username,t2.door_status,
		to_char(t2.survey_date::date,''DD/MM/YYYY'') record_datetime
		FROM pcmc_survey.building_w_grid_id_3857 t1
		JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
		WHERE --t2.door_status is not null and 
        t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null)

	SELECT count(distinct (gis_id)) cmpled_bild_pen,username  FROM (
		SELECT gis_id,username FROM (
		SELECT gis_id,username,door_status, record_datetime FROM bld_summary_pen_in
		)q1 WHERE '||date_lesseq_cond||'
	)q2 GROUP BY username
),
bld_total as (SELECT count(building_i) total_building,username FROM (
	SELECT distinct on (t1.building_i) building_i,t1.id,t2.username
	FROM pcmc_survey.building_w_grid_id_3857 t1
	LEFT JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id ORDER BY t1.building_i
)q1  GROUP BY username),

prop_summary as (WITH prop_summary_in AS (SELECT t3.property_no,t2.gis_id,t3.username,t3.door_status,
to_char(t3.survey_date::date,''DD/MM/YYYY'') record_datetime
FROM pcmc_survey.building_w_grid_id_3857 t1
JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
WHERE --t3.door_status is not null and
 t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null)

SELECT count(distinct ( property_no,gis_id)) cmpled_pro,username FROM (
SELECT property_no,gis_id,username FROM (
SELECT property_no,gis_id,username,door_status, record_datetime FROM prop_summary_in
)q1 WHERE '||date_cond||'
)q2 GROUP BY username ),

prop_summary_pen as (WITH prop_summary_pen_in AS (SELECT t3.property_no,t2.gis_id,t3.username,t3.door_status,
to_char(t3.survey_date::date,''DD/MM/YYYY'') record_datetime
FROM pcmc_survey.building_w_grid_id_3857 t1
JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
WHERE --t3.door_status is not null and 
t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null)

SELECT count(distinct ( property_no,gis_id)) cmpled_pro_pen,username FROM (
SELECT property_no,gis_id,username FROM (
SELECT property_no,gis_id,username,door_status, record_datetime FROM prop_summary_pen_in
)q1 WHERE '||date_lesseq_cond||'
)q2 GROUP BY username ),

prop_total as (
SELECT COALESCE(SUM(total_properties),0) total_property,username FROM (
SELECT distinct on (t1.building_i) building_i,t1.id,t3.username,
(COALESCE(t2.total_properties,0)) total_properties
FROM pcmc_survey.building_w_grid_id_3857 t1
LEFT JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
LEFT JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
)q1 GROUP BY username )

SELECT json_agg(json_build_object(''name'',name,
''completed_building'',completed_building,
''pending_building'',pending_building,
''total_building'',total_building,
''total_property'',total_property,
''completed_property'',completed_property,
''pending_property'',pending_property))
FROM (SELECT username as name, 
SUM(cmpled_bild) completed_building,
SUM(pending_bld) pending_building,
SUM(total_building) total_building,
SUM(total_property) total_property,
SUM(cmpled_pro) completed_property,
SUM(pending_pro) pending_property FROM
(SELECT --t2.username,
btrim(concat(initcap(z2.cn::text), '' '', initcap(z2.sn::text))) username,
COALESCE (t1.cmpled_bild,0) cmpled_bild,
0 total_building,
0 pending_bld,
0 total_property,
COALESCE (t3.cmpled_pro,0) cmpled_pro,
0 pending_pro
FROM adm.tbl_users z1
 JOIN adm.tbl_ldap_users z2 on z1.user_name = z2.user_name
 JOIN bld_total t2 on z1.user_name = t2.username
LEFT JOIN bld_summary t1 ON t2.username = t1.username
LEFT JOIN bld_summary_pen t5 ON t2.username = t5.username
LEFT JOIN prop_total t4 ON t2.username = t4.username
LEFT JOIN prop_summary t3 ON t4.username = t3.username 
LEFT JOIN prop_summary_pen t6 ON t4.username = t6.username 
ORDER BY 4 desc)z1 WHERE username IS NOT NULL  GROUP BY username)z5';
			EXECUTE summary_grid_input INTO summary_grid;
			RAISE NOTICE 'final summary_grid_input - %', summary_grid_input;
			RETURN JSON_BUILD_OBJECT('data', summary_grid);
ELSE

summary_grid_input := 
'WITH bld_summary as (WITH bld_summary_in AS (
		SELECT t2.gis_id,t1.zone_no,t1.gut_no,t1.grid_id,t2.door_status,
		to_char(t2.survey_date::date,''DD/MM/YYYY'') record_datetime
		FROM pcmc_survey.building_w_grid_id_3857 t1
		JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
		WHERE --t2.door_status is not null and 
        t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null)
		SELECT count(distinct (gis_id)) cmpled_bild,zone_no,gut_no,grid_id  FROM (
	SELECT gis_id,zone_no,gut_no,grid_id FROM (
		SELECT gis_id,zone_no,gut_no,grid_id,door_status, record_datetime FROM bld_summary_in
		)q1 WHERE '||date_cond||'
	)q2 GROUP BY zone_no,gut_no,grid_id
),
bld_summary_pen as (WITH bld_summary_pen_in AS (
		SELECT t2.gis_id,t1.zone_no,t1.gut_no,t1.grid_id,t2.door_status,
		to_char(t2.survey_date::date,''DD/MM/YYYY'') record_datetime
		FROM pcmc_survey.building_w_grid_id_3857 t1
		JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
		WHERE --t2.door_status is not null and 
        t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null)

SELECT count(distinct (gis_id)) cmpled_bild_pen,zone_no,gut_no,grid_id  FROM (
		SELECT gis_id,zone_no,gut_no,grid_id FROM (
		SELECT gis_id,zone_no,gut_no,grid_id,door_status,record_datetime FROM bld_summary_pen_in
		)q1 WHERE '||date_lesseq_cond||'
	)q2 GROUP BY zone_no,gut_no,grid_id
),
bld_total as (SELECT count(building_i) total_building,zone_no,gut_no,grid_id FROM (
	SELECT distinct on (t1.building_i) building_i,t1.id,t1.zone_no,t1.gut_no,t1.grid_id
	FROM pcmc_survey.building_w_grid_id_3857 t1
	LEFT JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id --ORDER BY t1.building_i
	WHERE t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null
)q1  GROUP BY zone_no,gut_no,grid_id),

prop_summary as (
WITH prop_summary_in AS (SELECT t3.property_no,t2.gis_id,t1.zone_no,t1.gut_no,t1.grid_id,t3.door_status,
to_char(t3.survey_date::date,''DD/MM/YYYY'') record_datetime
FROM pcmc_survey.building_w_grid_id_3857 t1
JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
WHERE --t3.door_status is not null and 
t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null)

SELECT count(distinct ( property_no,gis_id)) cmpled_pro,zone_no,gut_no,grid_id FROM (
SELECT property_no,gis_id,zone_no,gut_no,grid_id FROM (
SELECT property_no,gis_id,zone_no,gut_no,grid_id,door_status, record_datetime FROM prop_summary_in
)q1 WHERE '||date_cond||'
)q2 GROUP BY zone_no,gut_no,grid_id ),

prop_summary_pen as (WITH prop_summary_pen_in AS (SELECT t3.property_no,t2.gis_id,t1.zone_no,t1.gut_no,t1.grid_id,t3.door_status,
to_char(t3.survey_date::date,''DD/MM/YYYY'') record_datetime
FROM pcmc_survey.building_w_grid_id_3857 t1
JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
WHERE --t3.door_status is not null and 
t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null)

SELECT count(distinct ( property_no,gis_id)) cmpled_pro_pen,zone_no,gut_no,grid_id FROM (
SELECT property_no,gis_id,zone_no,gut_no,grid_id FROM (
SELECT property_no,gis_id,zone_no,gut_no,grid_id,door_status, record_datetime FROM prop_summary_pen_in
)q1 WHERE '||date_lesseq_cond||'
)q2 GROUP BY zone_no,gut_no,grid_id ),

prop_total as (
SELECT COALESCE(SUM(total_properties),0) total_property,zone_no,gut_no,grid_id FROM (
SELECT distinct on (t1.building_i) building_i,t1.id,t1.zone_no,t1.gut_no,t1.grid_id,
(COALESCE(t2.total_properties,0)) total_properties
FROM pcmc_survey.building_w_grid_id_3857 t1
LEFT JOIN pcmc_survey.buildings_others_en t2 ON t1.building_i = t2.gis_id
LEFT JOIN pcmc_survey.property_others_en t3 ON t2.gis_id = t3.gis_id
WHERE t1.zone_no is not null and t1.gut_no is not null and t1.grid_id is not null
)q1 GROUP BY zone_no,gut_no,grid_id )

SELECT json_agg(json_build_object(''name'','||quote_literal(alias_name)||'||name,
''type_no'',name,
''completed_building'',completed_building,
''pending_building'',pending_building,
''total_building'',total_building,
''total_property'',total_property,
''completed_property'',completed_property,
''pending_property'',pending_property))
FROM (SELECT '||col_name||' as name, SUM(cmpled_bild) completed_building,
SUM(pending_bld) pending_building,
SUM(total_building) total_building,
SUM(total_property) total_property,
SUM(cmpled_pro) completed_property,
SUM(pending_pro) pending_property FROM
(SELECT t2.zone_no,t2.gut_no,t2.grid_id,COALESCE (t1.cmpled_bild,0) cmpled_bild,t2.total_building,
(COALESCE (t2.total_building,0) - COALESCE (t5.cmpled_bild_pen,0)) pending_bld,
COALESCE (t4.total_property,0) total_property,
COALESCE (t3.cmpled_pro,0) cmpled_pro,
(COALESCE (t4.total_property,0) - COALESCE (t6.cmpled_pro_pen,0)) pending_pro
FROM bld_total t2 
LEFT JOIN bld_summary t1 ON t2.zone_no = t1.zone_no AND t2.gut_no = t1.gut_no AND t2.grid_id = t1.grid_id
LEFT JOIN bld_summary_pen t5 ON t2.zone_no = t5.zone_no AND t2.gut_no = t5.gut_no AND t2.grid_id = t5.grid_id
LEFT JOIN prop_total t4 ON t2.zone_no = t4.zone_no AND t2.gut_no = t4.gut_no AND t2.grid_id = t4.grid_id
LEFT JOIN prop_summary t3 ON t4.zone_no = t3.zone_no AND t4.gut_no = t3.gut_no AND t4.grid_id = t3.grid_id
LEFT JOIN prop_summary_pen t6 ON t4.zone_no = t6.zone_no AND t4.gut_no = t6.gut_no AND t4.grid_id = t6.grid_id
ORDER BY 4 desc)z1 WHERE '||col_name||' IS NOT NULL  GROUP BY '||col_name||')z5';
			EXECUTE summary_grid_input INTO summary_grid;
			RAISE NOTICE 'final summary_grid_input - %', summary_grid_input;
			RETURN JSON_BUILD_OBJECT('data', summary_grid);
		END IF;
		RAISE NOTICE 'org issssssssssssssssssssssssssssssssssss';
	ELSE
		RAISE NOTICE 'org issssssssssssssssssssssssssssssssssss';
		--RETURN json_build_object ( 'data',  graph_data);
	END IF;
END;
$BODY$;
