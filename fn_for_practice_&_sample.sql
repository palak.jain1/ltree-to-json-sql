CREATE OR REPLACE FUNCTION adminportal.fn_for_practice(
	datadetail text)
    RETURNS text
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE SECURITY DEFINER PARALLEL UNSAFE
AS $BODY$
-- SELECT adminportal.fn_for_practice( '{"org_id":6}' )
-- select * from adm.tbl_organization
DECLARE  -- declare variable
 		 
	datadetails json;
	org_id int;
	qry text;
	qry_ot json;
	
	ext text;
	ext_ot boolean;	
BEGIN

	datadetails := datadetail::json;
	--raise notice 'input_param %', datadetails;
	org_id :=  (datadetails->>'org_id')::int;
	--raise notice 'org_id - %', org_id; 
	
	ext := 'select exists (select true from adm.tbl_organization where org_id = '||org_id||')';
	EXECUTE ext INTO ext_ot;
	--raise notice 'ext_ot %', ext_ot;
	
IF (ext_ot is false) THEN 
			--raise notice 'org is not exist';
	RETURN  'org is not exist';
ELSE
	qry := 'SELECT json_build_object(''org_id'',org_id,
								  ''org_name'',org_name,
			                      ''org_address'',org_address,
								  ''org_phone'',org_phone,
			                      ''org_email'',org_email) 
			FROM adm.tbl_organization WHERE org_id = '||org_id||'';
			
	EXECUTE qry INTO qry_ot;
	
	RETURN json_build_object ( 'data',  qry_ot);
END IF;			 
END;
$BODY$;
