
This command will give you postgres port number

 \conninfo
If postgres is running on Linux server, you can also use the following command

sudo netstat -plunt |grep postgres
OR (if it comes as postmaster)

sudo netstat -plunt |grep postmaster
and you will see something similar as this

tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      140/postgres
tcp6       0      0 ::1:5432                :::*                    LISTEN      140/postgres
in this case, port number is 5432 which is also default port number
