--To temporarily disable all triggers in a PostgreSQL session, use this:

SET session_replication_role = replica;

--That disables all triggers for the current database session only. Useful for bulk operations, but remember to --be careful to keep your database consistent.

--To re-enable:

SET session_replication_role = DEFAULT;

=======================================================================
-- in single function disable and enable some specific trigger
-- alter table table_name disable trigger trigger_name

    ALTER TABLE adm.tbl_org_cat_rel DISABLE TRIGGER tfn_create_partition_tables;
    ALTER TABLE adm.tbl_dept_cat_rel DISABLE TRIGGER create_cat_view;
    --sql statement 
    --sql statement 
    ALTER TABLE adm.tbl_org_cat_rel ENABLE TRIGGER tfn_create_partition_tables;
    ALTER TABLE adm.tbl_dept_cat_rel ENABLE TRIGGER create_cat_view;
