

--Command-line control login remotely from another computer


--You can use ssh. You'll need to install an ssh server on the remote machine:

sudo apt-get install openssh-server  


--You can then connect to the computer with:

ssh username@remote-pcs-ip
