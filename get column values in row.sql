select distinct
         gis_id,
         unnest(array[[['garden', 'gis_id', 'gut_no']]]) as col_name, -- column  name into row 
         unnest(array[[garden::text, gis_id::text, gut_no::text]]) as col_val -- column  value
from pcmc_survey.tbl_buildings 
