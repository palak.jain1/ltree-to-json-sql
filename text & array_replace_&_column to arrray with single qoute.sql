SELECT array_replace(ARRAY[1,2,5,4], 5, 3);
SELECT array_replace(ARRAY['foo','bar','baz','bar'], 'bar', 'baz');

select string_agg(''''|| column_name ||'''', ',') from information_schema.columns where table_name = 'tbl_poly_2_46' and table_schema = 'spatial'


--------------------------------text- with out single qoute we need qoute literal--------------------------------
WITH tbl_main as (select string_agg(column_name, ',') col_list from information_schema.columns 
where table_name = 'tbl_poly_2_46' and table_schema = 'spatial')
SELECT REPLACE(col_list,'category_id', 'category_iddddd') FROM tbl_main


--------------------------------text- with single qoute we need qoute literal--------------------------------
WITH tbl_main as (select string_agg(quote_literal(column_name), ',') col_list from information_schema.columns 
where table_name = 'tbl_poly_2_46' and table_schema = 'spatial')
SELECT REPLACE(col_list,'category_id', 'category_iddddd') FROM tbl_main

