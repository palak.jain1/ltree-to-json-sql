--Search text of all functions in PGAdmin

select proname,prosrc from pg_proc where prosrc like '%access%'


----------------by prosrc--------------------------------

select * from pg_proc where prosrc like '%SELECT case when %';

----------------by information_schema--------------------------------
select 
	routine_catalog AS DatabaseName
	,routine_schema AS SchemaName
	,routine_name AS FunctionName
	,routine_type AS ObjectType
from information_schema.routines 
where routine_definition like '%SELECT case when%'

----------------------ankit method----------------------------------
select n.nspname as function_schema,
p.proname as function_name,
l.lanname as function_language,
case when l.lanname = 'internal' then p.prosrc
else pg_get_functiondef(p.oid)
end as definition,
pg_get_function_arguments(p.oid) as function_arguments,
t.typname as return_type
from pg_proc p
left join pg_namespace n on p.pronamespace = n.oid
left join pg_language l on p.prolang = l.oid
left join pg_type t on t.oid = p.prorettype
where n.nspname not in ('pg_catalog', 'information_schema')
--and n.nspname='trex'
and prosrc ilike '%fn_gs_create_datastore%'
order by function_schema,
function_name;
