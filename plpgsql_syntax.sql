/*
----------------------------------------------------------
Author : XXXXXXXXXX
Purpose : XXXXXXXXXXXXXXXXXXX
Logic Structure:
<use this area to document the basic
flow of the program at a high level. You
should also make notes about anything else
that might help someone reading your code.>
Modification History:
Date Name Revision Summary
---------- ---------- ------------------------------------
/*

	**********************Creation Details**********************
	Stored Procedure Name : udf_FunctionName
	Purpose : Calculates and returns the value.
	Author : Author Name
	Created On : 2017/01/01
	*****************************Revision Details*****************************
	Project/
	Revision No. Changed On  Changed By  Change Description
	------------ ----------  ----------  ------------------
	1234         2018/01/01   Mr. ABC    Changed the calculation part.
	1235         2018/02/08   Mr. XYZ    Revert the previous change.
*/
----------------------------------------------------------
*/
/*
Rule	Description
1	    Keywords and names are written in lowercase1.
2	    2 space indention.
3	    One command per line.
4	    Keywords loop, else, elsif, end if, when on a new line.
5	    Commas in front of separated elements.
6	    Call parameters aligned, operators aligned, values aligned.
7	    SQL keywords are right aligned within a SQL command.
8	    Within a program unit only line comments -- are used.
9	    Brackets are used when needed or when helpful to clarify a construct
*/

------------------------------------------------------------------




