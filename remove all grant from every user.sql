create or replace function aa_practice.remove_user_test()
returns boolean
language plpgsql
as
$$
/*********************************************************************************************************
NAME     :  aa_practice.remove_user_test
PURPOSE  :  remove grant.
REVISIONS:
Ver        Date Author                   Description

---------  ----------       ----------------------------- ----------------------------

1.0        2021-08-10        AnkitSonani       1. Initial Version.
1.1		   2021-08-18 		 PalakJain		   1.1 

select aa_practice.remove_user_test()

GRANT ALL ON TABLE adm.tbl_access_groups TO "AdityaSaraswat4362";
GRANT ALL ON TABLE adm.tbl_access_master TO "AdityaSaraswat4362";
GRANT ALL ON TABLE adm.vw_access_user_rel_orgid_2_accid_74 TO "AdityaSaraswat4362";
GRANT ALL ON TABLE spatial.tbl_admin_layers TO "AdityaSaraswat4362";

GRANT ALL ON SCHEMA adm TO "AdityaSaraswat4362";
GRANT ALL ON SCHEMA access_rel TO "AdityaSaraswat4362";
GRANT ALL ON SCHEMA aa_practice TO "AdityaSaraswat4362";

SELECT current_user;
SELECT session_user, current_user;
SET SESSION AUTHORIZATION 'gs_admin';

IF want grantor name then just add grantor in the column 
*************************************************************************************************************/
DECLARE
 _rec record; 
 _qry text;
 
BEGIN  

CREATE TEMPORARY TABLE tmp_user_name AS 
	SELECT 'table' as typee, 
			grantee, 
			table_name 
	FROM (
			SELECT 
				DISTINCT grantee, table_schema || '.' || table_name as table_name 
			FROM
			   information_schema.role_table_grants 
			WHERE
			   grantee NOT IN  ('postgres','sys_adm','internal_owner','internal_users','gs_admin','rw_group','PUBLIC')
		)q1
	UNION ALL   
	SELECT 'schema' as typee, 
			grantee, 
			table_name 
	FROM (
			SELECT
				DISTINCT grantee,
				nspname as table_name
			FROM(
				  SELECT  
						r.usename as grantor, 
						e.usename as grantee, 
						nspname, 
						privilege_type, 
						is_grantable
					FROM pg_namespace
					JOIN lateral ( SELECT * FROM aclexplode(nspacl) AS x ) a ON true
					JOIN pg_user e ON a.grantee = e.usesysid
					JOIN pg_user r ON a.grantor = r.usesysid  
				  WHERE 
					e.usename NOT IN ('postgres','sys_adm','internal_owner','internal_users','gs_admin','rw_group','PUBLIC')
				) q1 
		)q2;
		
	FOR _rec IN SELECT * FROM  tmp_user_name    
		 LOOP
				IF (_rec.typee = 'schema') THEN
					_qry := 'REVOKE ALL ON SCHEMA '||_rec.table_name||' FROM "'||_rec.grantee||'"';
					EXECUTE _qry;
					raise notice '_qry %',_qry;
				ELSE
					_qry := 'REVOKE ALL PRIVILEGES ON TABLE '||_rec.table_name||' FROM "'||_rec.grantee||'"';
					EXECUTE _qry;
					raise notice '_qry %',_qry;

				END IF;
		 END LOOP;

	DROP TABLE tmp_user_name;
	
	RETURN true;
END;
$$;
